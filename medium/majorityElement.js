console.log(majorityElement([1, 2]));

function majorityElement(nums) {
  let hash = {},
    res = [];
  nums.forEach((element) => {
    hash[element] = hash[element] ? hash[element] + 1 : 1;
  });

  Object.keys(hash).forEach((key) => {
    if (hash[key] > nums.length / 3) {
      res.push(key);
    }
  });

  return res;
}
