console.log(smallestSubsequence("basda"));

function smallestSubsequence(s) {
  let map = {};
  let res = [];
  let strs = s.split("");

  strs.forEach((l) => {
    map[l] = map[l] ? map[l] + 1 : 1;
  });

  Object.keys(map).forEach((key) => {
    if (map[key] == 1) {
      res.push(key);
    }
  });

  console.log(strs);
  return res;
}
