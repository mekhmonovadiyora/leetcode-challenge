console.log(countPrimes(5000000));

function countPrimes(n) {
  let count = 0;

  function isPrime(x) {
    if (x <= 1) return false;
    else if (x <= 3) return true;
    else if (x % 2 == 0 || x % 3 == 0) return false;

    for (let i = 5; i * i <= x; i++) {
      if (x % i == 0) {
        return false;
      }
    }
    return true;
  }

  for (let j = 0; j < n; j++) {
    if (isPrime(j)) {
      count++;
    }
  }

  return count;
}
