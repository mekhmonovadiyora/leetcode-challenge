console.log(sortArray([5, 3, 8, 1, 9, 43, 432]));

function sortArray(nums) {
  if (nums.length < 2) {
    return nums;
  }

  let pivot = nums[0];
  let less = [];
  let greater = [];
  for (let i = 1; i < nums.length; i++) {
    if (nums[i] > pivot) {
      greater.push(nums[i]);
    } else {
      less.push(nums[i]);
    }
  }

  return [...sortArray(less), pivot, ...sortArray(greater)];
}
