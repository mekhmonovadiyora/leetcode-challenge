console.log(longestPalindrome("ac"));

function longestPalindrome(s) {
  if (s.length == 1) return s;
  let longlest = "";

  for (let i = 0; i < s.length; i++) {
    let current = s[i];
    for (let j = i + 1; j < s.length; j++) {
      current += s[j];

      let arr = current.split("");
      arr.reverse();
      arr = arr.join("");

      if (current == arr) {
        if (current.length > longlest.length) {
          longlest = current;
        } else {
          longlest = s[i];
        }
      }
    }
  }
  return longlest;
}
