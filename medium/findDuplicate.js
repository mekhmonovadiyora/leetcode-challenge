console.log(findDuplicate([2, 2, 2, 2, 2]));

function findDuplicate(nums) {
  let n = nums.length - 1;
  let actualSum = (parseFloat(1 + nums[n]) / 2.0) * n;
  let sum = 0;

  nums.forEach((n) => {
    sum += n;
  });

  //   console.log("actual", actualSum);
  //   console.log("sum", sum);

  return sum - actualSum;
}
