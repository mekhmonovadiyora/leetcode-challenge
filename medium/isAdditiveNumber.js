console.log(isAdditiveNumber("12350"));

function isAdditiveNumber(num) {
  if (num.length < 3) {
    return false;
  }

  let digits = num.split("");

  for (let i = 2; i < digits.length; i++) {
    if (Number(digits[i - 2]) + Number(digits[i - 1]) !== Number(digits[i])) {
      return false;
    }
  }

  return true;
}
