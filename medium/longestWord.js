console.log(
  longestWord(["a", "banana", "app", "appl", "ap", "apply", "apple"])
);

function longestWord(words) {
  let hashing = {};
  words.forEach((element) => {
    hashing[element.length] = hashing[element.length]
      ? [...hashing[element.length], element]
      : [element];
  });

  return hashing;
}
