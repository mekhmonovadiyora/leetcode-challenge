console.log(integerReplacement(65535));

function integerReplacement(n) {
  let count = 0;
  if (n == 1) {
    return 0;
  }

  while (n != 1) {
    console.log(n);
    if (n % 2 == 0) {
      n = n / 2;
      count++;
    } else {
      if (n + 1 < n - 1) {
        n = n + 1;
        count++;
      } else {
        n = n - 1;
        count++;
      }
    }
  }
  return count;
}
