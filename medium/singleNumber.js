console.log(singleNumber([1, 2, 1, 1]));

function singleNumber(nums) {
  let map = {},
    res;
  nums.forEach((el) => {
    map[el] = map[el] ? map[el] + 1 : 1;
  });

  Object.keys(map).forEach((key) => {
    if (map[key] == 1) {
      res = key;
    }
  });
  return res;
}
