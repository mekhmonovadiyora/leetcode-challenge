console.log(findComplement(5));

function findComplement(num) {
  let r;
  let s = "";
  let s1 = "";
  let dec = 0;
  let k = 0;

  while (num > 0) {
    r = num % 2;
    s += r.toString();
    num = Math.trunc(num / 2);
  }

  for (let i = s.length - 1; i >= 0; i--) {
    if (s[i] == "0") {
      s1 += "1";
    } else {
      s1 += "0";
    }
  }

  for (let i = s1.length - 1; i >= 0; i--) {
    dec += Number(s1[i] * Math.pow(2, k));
    k++;
  }
  return dec;
}
