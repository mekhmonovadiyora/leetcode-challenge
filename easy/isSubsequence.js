console.log(isSubsequence("abc", "ahbgdc"));

function isSubsequence(s, t) {
  let sHash = {},
    tHash = {},
    res = true;

  for (let i = 0; i < s.length; i++) {
    sHash[s[i]] = i;
  }

  for (let j = 0; j < t.length; j++) {
    tHash[t[j]] = j;
  }

  Object.keys(sHash).forEach((hash) => {
    if (!tHash[hash] || sHash[hash] > tHash[hash]) {
      console.log(sHash[hash]);
      console.log(tHash[hash]);
      res = false;
    }
  });

  console.log(sHash);
  console.log(tHash);

  return res;
}
