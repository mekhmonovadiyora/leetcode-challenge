console.log(singleNumber([2, 2, 1, 1, 3]));

function singleNumber(nums) {
  let hashmap = {},
    res;
  nums.forEach((n) => {
    hashmap[n] = hashmap[n] ? hashmap[n] + 1 : 1;
  });

  Object.keys(hashmap).forEach((n) => {
    if (hashmap[n] == 1) {
      res = n;
    }
  });
  return res;
}
