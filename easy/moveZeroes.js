console.log(moveZeroes([0, 0, 1]));

function moveZeroes(nums) {
  let count = 0;
  let n = nums.length;
  for (let i = 0; i < n; i++) {
    if (nums[i] == 0 ?? nums[i - 1] == 0) {
      count++;
      nums.splice(i, 1);
      i--;
    }
  }
  for (let i = 1; i <= count; i++) {
    nums.push(0);
  }
  return nums;
}
