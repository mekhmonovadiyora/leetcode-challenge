console.log(
  averageValue([
    94, 65, 82, 40, 79, 74, 92, 84, 37, 19, 16, 85, 20, 79, 25, 89, 55, 67, 84,
    3, 79, 38, 16, 44, 2, 54, 58, 94, 69, 71, 14, 24, 13, 21,
  ])
);

function averageValue(nums) {
  let avr = 0;
  let count = 0;
  for (n of nums) {
    if (n % 2 == 0 && n % 3 == 0) {
      avr += n;
      count++;
    }
  }
  console.log(avr);
  console.log(count);

  if (avr) {
    avr = Math.round(avr / count);
  }

  return avr;
}
