console.log(dominantIndex([3, 6, 1, 0]));

function dominantIndex(nums) {
  let max = 0,
    res = true;
  for (let i = 0; i < nums.length; i++) {
    if (nums[i] > nums[max]) {
      max = i;
    }
  }

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] != nums[max]) {
      if (!(nums[max] >= nums[i] * 2)) {
        res = false;
      }
    }
  }
  return res ? max : -1;
}
