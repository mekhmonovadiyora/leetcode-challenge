console.log(lengthOfLongestSubstring("abcabcd"));

function lengthOfLongestSubstring(s) {
  let lcs = "";
  for (let i = 0; i < s.length; i++) {
    if (s[i] != s?.[i + 1]) {
      lcs += s[i];
      console.log(lcs);
    } else {
      return lcs;
    }
  }
}
