console.log(missingNumber([3, 1, 0]));

function missingNumber(nums) {
  let sum = ((1 + nums.length) / 2) * nums.length;
  let tempSum = 0;

  for (let i = 0; i < nums.length; i++) {
    tempSum = tempSum + nums[i];
  }
  return sum - tempSum;
}
