console.log(mySqrt(26));

function mySqrt(x) {
  for (let i = 1; i <= x; i++) {
    if (i * i == x) {
      return i;
    } else if (i * i < x && (i + 1) * (i + 1) > x) {
      return i;
    }
  }
  return 0;
}
