console.log(searchInsert([1, 3, 4, 8], 9));

function searchInsert(nums, target) {
  if (target < nums[0]) return 0;

  let start = 0,
    end = nums.length - 1;

  while (start <= end) {
    let mid = Math.floor((start + end) / 2);
    console.log("start", start);
    console.log("end", end);
    console.log("mid", mid);

    if (nums[mid] == target) {
      console.log("bingo");
      return mid;
    } else if (nums[mid] < target && nums[mid + 1] > target) {
      console.log("3 if");
      return mid + 1;
    } else if (nums[mid] < target) {
      console.log("1 if");
      start = mid + 1;
    } else if (nums[mid] > target) {
      console.log("2 if");
      end = mid - 1;
    } else if (nums[mid] < target && nums[mid + 1] > target) {
      console.log("3 if");
      return mid + 1;
    }
  }
  return nums.length;
}
