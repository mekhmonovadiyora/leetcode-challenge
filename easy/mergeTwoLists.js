mergeTwoLists([], [0]);

function mergeTwoLists(list1, list2) {
  let res = [];
  while (list1.length != 0 || list2.length != 0) {
    if (list1?.[0] < list2?.[0]) {
      res.push(list1[0]);
      list1.shift();
    }
    if (list2?.[0] < list1?.[0]) {
      res.push(list2[0]);
      list2.shift();
    }
    if (list1.length === 0) {
      res = res.concat(list2);
      list2 = [];
    }
    if (list2.length === 0) {
      list1 = [];
      res = res.concat(list1);
    }
  }
  return res;
}

// [1,4,7] [3,5,6]
// [4,7] [3,5,6] => [1]
// [4,7] [5,6] => [1,3]
