console.log(findErrorNums([1, 2, 2, 4]));

function findErrorNums(nums) {
  let map = {},
    sum = 0,
    actualSum = ((1 + nums.length) / 2) * nums.length,
    res = [];
  for (let i = 0; i < nums.length; i++) {
    if (map[nums[i]]) {
      map[nums[i]] = nums[i] + 1;
    } else {
      map[nums[i]] = 1;
    }
  }
  Object.keys(map).forEach((key) => {
    sum += Number(key);

    if (map[key] > 1) {
      res.push(key);
    }
  });
  res.push(actualSum - sum);
  return res;
}
