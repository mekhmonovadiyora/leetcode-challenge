console.log(removeDuplicates([1, 1, 2, 2, 2, 2, 3, 3, 3]));

function removeDuplicates(nums) {
  for (let i = 0; i < nums.length; i++) {
    if (nums?.[i - 1] === nums?.[i]) {
      nums.splice(i, 1);
      i -= 1;
    }
  }

  return nums;
}
