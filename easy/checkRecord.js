console.log(checkRecord("PPALLP"));

function checkRecord(s) {
  let absent = 0;
  let late = 0;

  for (let i = 0; i < s.length; i++) {
    if (s[i] == "A") absent++;

    if (s[i] == "L" && (s[i - 1] == "L" || s[i + 1] == "L")) late++;
    if (late < 3 && s[i + 1] != "L") late = 0;
  }
  if (late < 3 && absent < 2) {
    return true;
  }
  return false;
}
