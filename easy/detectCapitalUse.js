console.log(detectCapitalUse("lakeKK"));

function detectCapitalUse(word) {
  let res = 0;

  if (word[1] != word[1].toUpperCase()) {
    for (let i = 1; i < word.length; i++) {
      if (word[i] == word[i].toUpperCase()) {
        res -= 1;
      }
    }
  } else if (word[1] == word[1].toUpperCase()) {
    for (let j = 0; j < word.length; j++) {
      if (word[j] == word[j].toUpperCase()) {
        res += 1;
      }
    }
  }
  return res == 0 || res == word.length ? true : false;
}
