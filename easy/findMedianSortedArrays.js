console.log(findMedianSortedArrays([0, 0, 0, 0, 0], [-1, 0, 0, 0, 0, 0, 1]));

function findMedianSortedArrays(nums1, nums2) {
  let merged = [],
    i = 0,
    j = 0;

  while (merged.length != nums1.length + nums2.length) {
    if (nums1[i] < nums2[j]) {
      merged.push(nums1[i]);
      i++;
    } else if (nums2[j] < nums1[i]) {
      merged.push(nums2[j]);
      j++;
    } else if (nums1[i] == nums2[j]) {
      merged.push(nums1[i]);
      i++;
      j++;
    } else if (!nums1[i]) {
      merged = [...merged, nums2[j]];
      j++;
    } else if (!nums2[j]) {
      merged = [...merged, nums1[i]];
      i++;
    }
  }

  let n = merged.length;
  console.log(merged);

  if (n % 2 == 0) {
    return (merged[n / 2] + merged[n / 2 - 1]) / 2 || 0;
  } else {
    return merged[Number.parseFloat(Math.floor(n / 2))] || 0;
  }
}
