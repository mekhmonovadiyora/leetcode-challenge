console.log(removeElement([1, 0, 9, 0, 2, 3, 4, 0, 7], 0));

function removeElement(nums, val) {
  let i = 0;
  for (let j = 0; j < nums.length; j++) {
    if (nums[j] !== val) {
      nums[i] = nums[j];
      i++;
    }
  }
  return i, nums;
}
