console.log(findMaxConsecutiveOnes([1, 1, 0, 1, 1, 1]));

function findMaxConsecutiveOnes(nums) {
  let count = 0;
  let temp = 0;

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] == 1 && nums[i + 1] != 0) {
      temp++;
    }
    if (temp > count) {
      count = temp;
    }
  }
  return count;
}
