console.log(lengthOfLastWord(" i  have    a bug     "));

function lengthOfLastWord(s) {
  s = s.trim();
  let arr = s.split(" ");

  return arr[arr.length - 1].length;
}
