console.log(firstUniqChar("loveleetcode"));

function firstUniqChar(s) {
  for (let i = 0; i < s.length; i++) {
    let count = 0;
    let temp = s[i];
    for (let j = i; j < s.length; j++) {
      if (temp == s[j]) {
        count++;
      }
    }

    if (s[i] != s[i + 1] && s[i] != s[i - 1] && count == 1) {
      return i;
    }
  }
  return -1;
}
