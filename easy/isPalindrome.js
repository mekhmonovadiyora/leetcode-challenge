console.log(isPalindrome("A man, a plan, a canal: Panama"));

function isPalindrome(s) {
  s = s.toLowerCase();
  let newS = [];
  let arr = s.split("");

  for (let i = 0; i < arr.length; i++) {
    if (
      arr[i].toUpperCase() != arr[i].toLowerCase() ||
      (!isNaN(arr[i]) && arr[i] != " ")
    ) {
      newS.push(arr[i]);
    }
  }

  return newS.join("") == newS.reverse().join("");
}
