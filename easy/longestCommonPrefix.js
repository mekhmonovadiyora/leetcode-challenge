console.log(longestCommonPrefix(["flower", "flow", "flight"]));

function longestCommonPrefix(strs) {
  if (strs.length === 1) {
    return strs[0];
  }

  let prefix = "";

  for (let i = 0; i < strs[0].length; i++) {
    for (let j = 1; j < strs.length; j++) {
      if (!strs[j].startsWith(prefix + strs[0][i])) {
        return prefix;
      }
    }
    prefix += strs[0][i];
  }

  return prefix;
}
