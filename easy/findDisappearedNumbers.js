console.log(findDisappearedNumbers([4, 3, 2, 7, 8, 2, 3, 1]));

function findDisappearedNumbers(nums) {
  let res = [];
  let set = new Set();
  for (let num of nums) {
    if (!set.has(num)) {
      set.add(num);
    }
  }

  let n = 1;

  while (n <= nums.length) {
    if (!set.has(n)) {
      res.push(n);
    }
    n++;
  }

  return res;
}
