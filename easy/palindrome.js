console.log(isPalindrome(121));

function isPalindrome(x) {
  if (x < 0) return false;

  let a = 0;
  let b = x;
  let r = 0;

  while (x > 0) {
    r = x % 10;
    x = (x - r) / 10;
    a = a * 10 + r;
  }
  return a == b;
}
