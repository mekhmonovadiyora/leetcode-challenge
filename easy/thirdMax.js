console.log(thirdMax([3, 3, 4, 3, 4, 3, 0, 3, 3]));

function thirdMax(nums) {
  let maxIdx = 0;
  let max = nums[0];
  for (let j = 1; j <= 3; j++) {
    for (let i = 0; i < nums.length; i++) {
      if (nums[maxIdx] < nums[i]) {
        maxIdx = i;
        if (j == 1) max = nums[maxIdx];
      }
    }
    if (j != 3) {
      nums = nums.filter((n) => n != nums[maxIdx]);
      maxIdx = 0;
    }
  }
  console.log(nums[maxIdx]);
  return nums[maxIdx] ?? max;
}
