console.log(reverseWords("Let's take LeetCode contest"));

function reverseWords(s) {
  let words = s.split(" ");

  for (let i = 0; i < words.length; i++) {
    let current = "";

    for (let j = words[i].length - 1; j >= 0; j--) {
      current += words[i][j];
    }

    words[i] = current;
  }

  return words.join(" ");
}
