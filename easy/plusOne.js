console.log(plusOne([1, 2, 3]));

function plusOne(digits) {
  let a = 1;
  digits = digits.reverse();

  for (let i = 0; i < digits.length; i++) {
    let b = digits[i] + a;
    if (b > 9) {
      digits[i] = 0;
    } else {
      digits[i] = b;
      return digits.reverse();
    }
  }

  digits.push(a);

  return digits.reverse();
}
