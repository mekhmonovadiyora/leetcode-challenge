console.log(isPerfectSquare(25));

function isPerfectSquare(num) {
  let mid;
  low = 1;
  high = num;

  while (low <= high) {
    mid = Math.trunc((low + high) / 2);
    if (mid * mid == num) {
      return true;
    } else if (mid * mid > num) {
      high = mid - 1;
    } else {
      low = mid + 1;
    }
  }
  return false;
}
