console.log(search([1, 2, 3, 4, 5, 6, 7, 8], 9));

function search(nums, target) {
  let midIndex,
    lowIndex = 0,
    highIndex = nums.length - 1;

  while (lowIndex <= highIndex) {
    midIndex = Math.trunc((lowIndex + highIndex) / 2);

    if (nums[midIndex] === target) {
      return midIndex;
    } else if (nums[midIndex] > target) {
      highIndex = midIndex - 1;
    } else {
      lowIndex = midIndex + 1;
    }
  }

  return -1;
}
