console.log(isPowerOfThree(9));

function isPowerOfThree(n) {
  if (n <= 0) return false;
  while (true) {
    if (n == 1) {
      return true;
    }
    if (n % 3 !== 0) {
      return false;
    }

    n = n / 3;
  }
}
