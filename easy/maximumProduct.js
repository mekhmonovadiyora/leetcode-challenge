console.log(maximumProduct([1, 2, 3, 0]));

function maximumProduct(nums) {
  nums = sortArray(nums);
  function sortArray(arr) {
    if (arr.length < 2) {
      return arr;
    }

    let pivot = arr[0];
    let less = [];
    let greater = [];
    for (let i = 1; i < arr.length; i++) {
      if (arr[i] > pivot) {
        greater.push(arr[i]);
      } else {
        less.push(arr[i]);
      }
    }

    return [...sortArray(less), pivot, ...sortArray(greater)];
  }

  return nums[nums.length - 1] * nums[nums.length - 2] * nums[nums.length - 3];
}
