console.log(mostCommonWord("Bob", []));

function mostCommonWord(paragraph, banned) {
  if (
    paragraph[paragraph.length - 1].toLowerCase() ==
    paragraph[paragraph.length - 1].toUpperCase()
  ) {
    paragraph = paragraph.slice(0, paragraph.length - 1);
  }
  let arr = paragraph.split(" ");
  let hashmap = {};
  let bannedmap = {};

  banned.forEach((ban) => {
    bannedmap[ban] = ban;
  });

  arr.forEach((word) => {
    word = word.toLowerCase();
    if (
      word[word.length - 1].toLowerCase() == word[word.length - 1].toUpperCase()
    ) {
      word = word.slice(0, word.length - 1);
    }
    if (!bannedmap[word]) {
      hashmap[word] = hashmap[word] ? hashmap[word] + 1 : 1;
    }
  });

  let mostWord = Object.keys(hashmap)[0];
  Object.keys(hashmap).forEach((key) => {
    if (hashmap[key] > hashmap[mostWord]) {
      mostWord = key;
    }
  });
  return mostWord;
}
