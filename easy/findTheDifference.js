console.log(findTheDifference("abcd", "abcde"));

function findTheDifference(s, t) {
  let sMap = {},
    res;

  s.split("").forEach((el) => {
    sMap[el] = sMap[el] ? sMap[el] + 1 : 1;
  });

  t.split("").forEach((val) => {
    sMap[val] = sMap[val] ? sMap[val] - 1 : -1;
  });

  Object.keys(sMap).forEach((key) => {
    if (sMap[key] < 0) {
      res = key;
    }
  });

  return res;
}
