console.log(isPowerOfFour(1));

function isPowerOfFour(n) {
  if (n == 1) {
    return false;
  }
  while (n > 1) {
    n = n / 4;
  }

  return n == 1 ? true : false;
}
