console.log(reverseString(["h", "e", "y"]));

function reverseString(s) {
  for (let i = 0; i < s.length / 2; i++) {
    let n = s[i];
    s[i] = s[s.length - 1 - i];
    s[s.length - 1 - i] = n;
  }
  return s;
}
