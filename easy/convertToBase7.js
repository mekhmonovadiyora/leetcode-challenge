console.log(convertToBase7(7));

function convertToBase7(num) {
  if (num == 0) return "0";

  let r,
    isNegative = false;
  (res = ""), (base7 = "");

  if (num < 0) {
    num = -1 * num;
    isNegative = true;
  }

  while (num > 0) {
    r = num % 7;
    num = Math.trunc(num / 7);
    res += r;
  }

  for (let i = res.length - 1; i >= 0; i--) {
    base7 += res[i];
  }

  return isNegative ? (Number(base7) * -1).toString() : base7;
}
