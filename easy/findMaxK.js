console.log(findMaxK([1, -2, 3, -4, 4, -3]));

function findMaxK(nums) {
  let res = 0;

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] > res && nums.includes(nums[i] * -1)) {
      res = nums[i];
    }
  }
  return res != 0 ? res : -1;
}
