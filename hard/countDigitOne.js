console.log(countDigitOne(13));

function countDigitOne(n) {
  let count = 0;
  while (n > 0) {
    if (n % 10 == 1) {
      console.log("n", n);
      console.log("n%10", n % 10);
      count += 1;
    }
    n -= 1;
  }
  return count;
}
