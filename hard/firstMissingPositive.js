console.log(firstMissingPositive([-1, 1, 4, 6, 7, 2, 3, 5]));

function firstMissingPositive(nums) {
  let i = 1;
  const map = {};

  nums.forEach((el) => {
    map[el] = el;
  });

  while (true) {
    if (!map[i]) {
      return i;
    }
    i++;
  }
}
