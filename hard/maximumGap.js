console.log(maximumGap([3, 1, 4, 9, 5]));

function maximumGap(nums) {
  if (nums.length < 2) {
    return 0;
  }
  nums.sort((a, b) => a - b);
  let maxGap = nums[1] - nums[0];
  for (let i = 0; i < nums.length; i++) {
    if (nums[i] - nums[i - 1] > maxGap) {
      maxGap = nums[i] - nums[i - 1];
    }
  }
  return maxGap;
}
